package com.mzj.scala.base

/**
  * Created by Administrator on 2017/6/13.
  */
class Animal {



  protected  var config:String=""


  def this(config: String) { //辅助构造器(这里没有 val，var)
    this() //直接或间接调用主构造器==>生成对象 。
    this.config = config;
  }


}
