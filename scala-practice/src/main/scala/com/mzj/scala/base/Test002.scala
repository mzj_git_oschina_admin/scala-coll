package com.mzj.scala.base


/**
 * Created by Administrator on 2014/12/14.
 */
object Test002 {
  // 函数:可以使用def来定义一个函数。函数体是一个表达式。
  //使用Block表达式的时候，默认最后一行的返回是返回值，无需显式指定。
  //函数还可以像值一样，赋值给var或val。因此函数也可以作为参数传给另一个函数。
  def main(args: Array[String]) {

  }
  def square(a: Int) = a * a;
  def square2(a: Int) = { a * a }
  val squareVal = (a: Int) => a * a; //函数的另一种定义方式
  def addOne(f: Int => Int, arg: Int) = f(arg) + 1;//将函数作为参数传递

  println("square(2):" + square(2))
  println("squareWithBlock(2):" + square2(2))
  println("squareVal(2):" + squareVal(2))
  println("addOne(squareVal,2):" + addOne(squareVal, 2))
}
