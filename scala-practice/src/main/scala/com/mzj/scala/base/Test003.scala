package com.mzj.scala.base


/**
 * Created by Administrator on 2014/12/14.
 */
object Test003 {
  //借贷模式
  //由于函数可以像值一样作为参数传递，所以可以方便的实现借贷模式。
  //这个例子是从/proc/self/stat文件中读取当前进程的pid。
  //withScanner封装了try-finally块，所以调用者不用再close。
  //注：当表达式没有返回值时，默认返回Unit。
  def main(args: Array[String]) {
    var helloworld="hello"+"world" // var 定义变量
    val again=123 //val 定义常量，只能赋一次值
    println(helloworld)
    print(again)
  }
}
