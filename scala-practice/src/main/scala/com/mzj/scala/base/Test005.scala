package com.mzj.scala.base




/** Copyright 2015 TappingStone, Inc.
  *
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  *
  *     http://www.apache.org/licenses/LICENSE-2.0
  *
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  */

package io.prediction.workflow

import java.io.IOException
import java.net.ServerSocket
object Test005 {

  def main(args: Array[String]): Unit = {
    print(check(8080));

  }

  def check(port: Int): Int = {

    var effective_port = port;

    for (i <- 1 to 100) {
      try {
        val sock = new ServerSocket(effective_port);
        sock.close();
        return effective_port;
      } catch {
        case _: IOException => {
          print("%d 已经被占用", effective_port);
          effective_port += 1;
        };
      }
    }

    return port;//返回初始端口
  }

}

