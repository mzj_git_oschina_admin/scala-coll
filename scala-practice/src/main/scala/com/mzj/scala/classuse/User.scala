package com.mzj.scala.classuse

class User { //默认 public

  //方式一：自动生成get和set方法。
  // var userName="";

  //方式二：自定义get和set方法。
  private var privateuserName = ""; //必须初始化
  def userName = privateuserName;
  def userName_=(newUserName: String) {
    privateuserName = newUserName;
  }

}

object obj {
  def withClose(closeAble: { def close(): Unit }, op: { def close(): Unit } => Unit) {
    try {
      op(closeAble)
    } finally {
      closeAble.close()
    }
  }

  class Connection {
    def close() = println("close Connection")
  }

  val conn: Connection = new Connection()
  withClose(conn, conn => println("do something with Connection"))

}