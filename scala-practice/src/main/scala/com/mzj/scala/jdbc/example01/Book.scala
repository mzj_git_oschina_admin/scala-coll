package com.mzj.scala.jdbc.example01

import java.sql.Date

class Book extends Serializable {
  var id: Long = _
  var isbn: Long = _
  var name: String = _
  var date: Date = _

  def this(id: Long, isbn: Long, name: String, date: Date) {
    this();
    this.id = id;
    this.isbn = isbn;
    this.name = name;
    this.date = date;
  }

  override def toString(): String = {
    "id: " + id + "* name: " + name + " isbn: " + isbn + " date: " + date
  }
}