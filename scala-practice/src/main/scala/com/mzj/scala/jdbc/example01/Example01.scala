package com.mzj.scala.jdbc.example01
import java.sql.Connection
import java.sql.Statement
import java.sql.ResultSet
import java.sql.PreparedStatement
import org.apache.log4j.Logger
import org.apache.log4j.Logger
import org.apache.log4j.Logger
import org.apache.log4j.Logger
import org.apache.commons.lang3.exception.ExceptionUtils

object Example01 {
  def main(args: Array[String]): Unit = {
    val log = Logger.getLogger(this.getClass);
    val sql = """     select * from books    """

    var connection: Connection = null;
    var prepState: PreparedStatement = null;
    var rs: ResultSet = null;
    try {
      connection = ScalaDBCUtil.getConnection();
      prepState = connection.prepareStatement(sql);
      rs = prepState.executeQuery();

      var bookList = scala.collection.mutable.ArrayBuffer[Book]();
      while (rs.next()) {
        val book = new Book(rs.getInt("id"), rs.getInt("isbn"), rs.getString("name"), rs.getDate("date"));
        bookList += book;
      }
      bookList.foreach { book => println(book.toString()) };
    } catch {
      case t: Throwable =>
        log.error(ExceptionUtils.getStackTrace(t));
        throw t;
    } finally {
      ScalaDBCUtil.releaseResource(connection, prepState, rs);
    }

  }
}
