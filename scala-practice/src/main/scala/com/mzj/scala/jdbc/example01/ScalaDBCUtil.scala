package com.mzj.scala.jdbc.example01

import java.sql.DriverManager
import java.sql.Connection
import java.sql.Statement
import java.sql.ResultSet

object ScalaDBCUtil {
  val url = "jdbc:mysql://192.168.0.250:3306/test"
  val driver = "com.mysql.jdbc.Driver"
  val userName = "root"
  val userPassword = "123456"
  Class.forName(driver);

  /**
   * 获得连接对象
   */
  def getConnection(): Connection = {
    return DriverManager.getConnection(url, userName, userPassword);
  }

  /**
   * 释放资源
   */
  def releaseResource(connection: Connection, statement: Statement, resultSet: ResultSet): Unit = {
    if (resultSet != null) resultSet.close();
    if (statement != null) statement.close();
    if (connection != null) connection.close();
  }

}