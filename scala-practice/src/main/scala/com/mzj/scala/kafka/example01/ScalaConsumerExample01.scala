package com.mzj.scala.kafka.example01

import java.util.Properties
import kafka.consumer.ConsumerConfig
import kafka.consumer.Consumer
import kafka.consumer.Whitelist
import kafka.consumer.ConsumerIterator
import kafka.consumer.KafkaStream

class ScalaConsumer(val zookeeper: String,
                    val groupId: String,
                    val topic: String) {

  val consumer = Consumer.create(getConsumerConfig());

  /**
   * 获得consumerConfig对象:
   */
  def getConsumerConfig(): ConsumerConfig = {
    val props = new Properties();
    props.put("zookeeper.connect", zookeeper);
    props.put("group.id", groupId);
    props.put("auto.offset.reset", "largest");
    props.put("zookeeper.session.timeout.ms", "400");
    props.put("zookeeper.sync.time.ms", "200");
    props.put("auto.commit.interval.ms", "1000");
    return new ConsumerConfig(props);
  }

  /**
   * 获得数据——方式1：
   */
  def getMessageAndMetadata01(): Seq[KafkaStream[Array[Byte], Array[Byte]]] = {
    val whitelist = new Whitelist(topic); // 白名单
    return consumer.createMessageStreamsByFilter(whitelist);
  }

  /**
   * 获得数据——方式2：
   */
  def getMessageAndMetadata02(): Option[List[KafkaStream[Array[Byte], Array[Byte]]]] = {
    val topicCountMap = Map(topic -> 1);
    val consumerMap = consumer.createMessageStreams(topicCountMap);
    return consumerMap.get(topic);
  }

  /**
   * 释放资源：
   */
  def relaceResources(): Unit = {
    if (consumer != null) consumer.shutdown();
  }

}

object ScalaCustomerExample01 extends App {
  //  val zookeeper: String = "192.168.0.250:2181";
  val zookeeper: String = "10.48.193.247:2181";
  val groupId: String = "groupId0001";
  val topic: String = "test001";

  val scalaConsumer = new ScalaConsumer(zookeeper, groupId, topic);
  val streams = scalaConsumer.getMessageAndMetadata01;

  for (stream <- streams) {
    val it = stream.iterator();
    while (it.hasNext()) {
      val msg = new String(it.next().message());
      System.err.println(msg);
    }
  }

}


