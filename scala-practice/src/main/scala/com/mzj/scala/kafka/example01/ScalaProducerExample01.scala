package com.mzj.scala.kafka.example01

import java.util.Properties

import kafka.producer.KeyedMessage
import kafka.producer.Producer
import kafka.producer.ProducerConfig
/**
 * Copyright (C),HANDPAY<br>
 *
 * 生产者
 *
 * @author muzhongjiang
 * @date 2015年6月1日
 */
object ScalaProducerExample01 {
  val topic: String = "test001";

  /**
   * 获得produce对象:
   */
  def getProducer(): Producer[String, String] = {
    val props = new Properties()
    props.put("metadata.broker.list", "10.48.193.246:9092");
    props.put("serializer.class", "kafka.serializer.StringEncoder");
    props.put("producer.type", "async");

    val producerConfig = new ProducerConfig(props);
    return new Producer[String, String](producerConfig);
  }

  /**
   * 发送数据:
   */
  def sendMessage(producer: Producer[String, String], data: String): Unit = {
    val keyedMessage = new KeyedMessage[String, String](topic, data);
    producer.send(keyedMessage)
  }

  def main(args: Array[String]): Unit = {
    val data = "hello world";
    this.sendMessage(getProducer(), data);
  }

}
