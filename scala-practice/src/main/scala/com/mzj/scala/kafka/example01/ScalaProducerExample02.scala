package com.mzj.scala.kafka.example01

import java.util.Properties

import kafka.producer.KeyedMessage
import kafka.producer.Producer
import kafka.producer.ProducerConfig
/**
 * Copyright (C),HANDPAY<br>
 *
 * 发送数据列表
 *
 * @author muzhongjiang
 * @date 2015年6月1日
 */
object ScalaProducerExample02 {
  def main(args: Array[String]) {

    val brokers = "10.48.193.246:9092,10.48.193.247:9092,10.48.193.248:9092";
    val topic = "test001"
    val messagesTotal = 10; //多少行
    val wordsTotal = 5; //每行有多少个单词

    // Zookeeper connection properties
    val props = new Properties()
    props.put("metadata.broker.list", brokers)
    props.put("serializer.class", "kafka.serializer.StringEncoder");

    val config = new ProducerConfig(props);
    val producer = new Producer[String, String](config);

    // Send some messages
    while (true) {
      val messages = (1 to messagesTotal.toInt).map { messageNum =>
        val str = (1 to wordsTotal.toInt).map(x => scala.util.Random.nextInt(10).toString).mkString(" ");
        new KeyedMessage[String, String](topic, str);
      }.toArray

      producer.send(messages: _*) //发送message列表相当于List<Message>
      Thread.sleep(100)
    }
  }
}